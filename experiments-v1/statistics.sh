#!/bin/bash

filename="h_0.5-318581.out"
filename="h_0.5-323059.out"
filename="output_hybrid_ser.out"
rmlines=1

echo "Init time"
grep -nr "TimeInit" $filename | cut -d" " -f3 > auxfile
awk '{ total += $0 } END { print total/NR }' auxfile

echo "Clean time"
grep -nr "TimeRemoveDir" $filename | cut -d" " -f3 > auxfile
awk '{ total += $0 } END { print total/NR }' auxfile

echo "First simulation time"
grep -nr "TimeFirstSimulation" $filename | cut -d" " -f3 > auxfile
awk '{ total += $0 } END { print total/NR }' auxfile

echo "Dataset time"
grep -nr "TimeDataset" $filename | cut -d" " -f3 | tail -n +$rmlines > auxfile
awk '{ total += $0 } END { print total/NR }' auxfile

echo "Prediction time"
grep -nr "TimePrediction" $filename | cut -d" " -f3 | tail -n +$rmlines > auxfile
awk '{ total += $0 } END { print total/NR }' auxfile

echo "Regenerate time"
grep -nr "TimeRegenerate" $filename | cut -d" " -f3 | tail -n +$rmlines > auxfile
awk '{ total += $0 } END { print total/NR }' auxfile

echo "Simulation time"
#grep -nr "TimeIteration" $filename | cut -d" " -f3 > auxfile
grep -nr "TimeSimulation" $filename | cut -d" " -f3 | tail -n +$rmlines > auxfile
awk '{ total += $0 } END { print total/NR }' auxfile

echo "Last simulation time"
grep -nr "TimeLastSimulation" $filename | cut -d" " -f3 | tail -n +$rmlines > auxfile
awk '{ total += $0 } END { print total/NR }' auxfile

echo "Total time"
grep -nr "TimeTotal" $filename | cut -d" " -f3 > auxfile
awk '{ total += $0 } END { print total/NR }' auxfile

grep "NEXT TIMESTEP" $filename | tail -n1
