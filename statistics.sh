#!/bin/bash

filename="h_0.5-318581.out"
filename="h_0.5-323059.out"
filename="output_hybrid_ser.out"
filename=$1
rmlines=1


grep -nr "TimeInit" $filename | cut -d" " -f3 > auxfile
init=$(awk '{ total += $0 } END { print total/NR }' auxfile)
echo "Init time"
echo $init

grep -nr "TimeRemoveDir" $filename | cut -d" " -f3 > auxfile
clean=$(awk '{ total += $0 } END { print total/NR }' auxfile)
echo "Clean time"
echo $clean

grep -nr "TimeFirstSimulation" $filename | cut -d" " -f3 > auxfile
first=$(awk '{ total += $0 } END { print total/NR }' auxfile)
echo "First simulation time"
echo $first

grep -nr "TimeDataset" $filename | cut -d" " -f3 | tail -n +$rmlines > auxfile
gen=$(awk '{ total += $0 } END { print total/NR }' auxfile)
echo "Dataset time"
echo $gen

grep -nr "TimePrediction" $filename | cut -d" " -f3 | tail -n +$rmlines > auxfile
pred=$(awk '{ total += $0 } END { print total/NR }' auxfile)
echo "Prediction time"
echo $pred

grep -nr "TimeRegenerate" $filename | cut -d" " -f3 | tail -n +$rmlines > auxfile
reg=$(awk '{ total += $0 } END { print total/NR }' auxfile)
echo "Regenerate time"
echo $reg

echo "Simulation time"
#grep -nr "TimeIteration" $filename | cut -d" " -f3 > auxfile
grep -nr "TimeSimulation" $filename | cut -d" " -f3 | tail -n +$rmlines > auxfile
sim=$(awk '{ total += $0 } END { print total/NR }' auxfile)
echo $sim
it=$(awk '{ total += 1 } END { print total }' auxfile)

#echo "Last simulation time"
#grep -nr "TimeLastSimulation" $filename | cut -d" " -f3 | tail -n +$rmlines > auxfile
#awk '{ total += $0 } END { print total/NR }' auxfile

echo "Total time"
grep -nr "TimeTotal" $filename | cut -d" " -f3 > auxfile
awk '{ total += $0 } END { print total/NR }' auxfile

echo "ITERATIONS: $it"
grep "NEXT TIMESTEP" $filename | tail -n1
echo "sum time"
num=$(python -c "print($init-$clean+$first+$gen+$pred+$reg+($sim+$gen+$pred+$reg)*$it)")
echo $num